$(document).ready(function () {
    // When clicking on the hamburger menu the class 'open' will be toggled
    const header = $('.header');
    const body = $('body');
    const bmenu = $('#btnHamburger');
    const overlay = $('.overlay');
    const headerPopup = $('.header__popup')
    const project = $('.project');

    // --- Navigation bar
    // Hamburger menu open/close
    bmenu.on('click', (evt) => {

        if (header.hasClass("open")) {
            // Close it
            header.removeClass("open");
            overlay.removeClass("fade-in").addClass("fade-out");
            headerPopup.addClass('fade-out').removeClass('fade-in');
            // Enable scrolling
            body.css({ 'overflow': 'visible' })
        } else {
            // Open it
            header.addClass("open");
            overlay.removeClass("fade-out").addClass("fade-in");
            headerPopup.addClass('fade-in').removeClass('fade-out');
            // Disable scrolling
            body.css({ 'overflow': 'hidden' })
        }
    });

    // --- Hero section
    // Typing the text with my interests (for now, the whole string, just once)
    const typedTextElement = $('.typed-text');
    const typedText = "UX Research, UX Design, Academic Research, Illustration, Programming!"
    let i = 0;

    function typeWrite() {
        if (i < typedText.length) {
            str1 = typedTextElement.text();
            typedTextElement.text(str1 += typedText.charAt(i));
            i++;
            setTimeout(typeWrite, 80);
        }
    }

    // Runs once when loading the main page
    typeWrite();

    // --- Projects section
    project.hover(
        function () {
            if ($(window).width() > 1024) {
                $(this).find('div').addClass('fade-in').removeClass('fade-out');
            }
        },
        function () {
            if ($(window).width() > 1024) {
                $(this).find('div').removeClass('fade-in').addClass('fade-out');
            }
        }
    );

    // Scroll to top btn
    var btn = $('#up-button');
    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });
    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, '300');
    });

    $(document).bind("contextmenu", function (e) {
        e.preventDefault();
    });
    
    $(window).on("keydown", function (e) {
        if (e.keyCode == 123) {
            return false;
        }
        if(e.keyCode == 'F12'.charCodeAt(0)){
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'E'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'H'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'A'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'F'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'E'.charCodeAt(0)) {
            return false;
        }
    });
});


