# Alessandra Portfolio

Link: https://alessandra-semeraro-portfolio.netlify.app/

My portfolio for job applications.

# Some notes on how to hide the .html from Netlify
https://medium.com/@towfiqu/removing-html-extension-from-netlify-hosted-static-site-with-parcel-41481c9e32fd

# Breakpoints (think about adding)
    --breakpoint-xs: 0;
    --breakpoint-sm: 576px;
    --breakpoint-md: 768px;
    --breakpoint-lg: 992px;
    --breakpoint-xl: 1200px;
